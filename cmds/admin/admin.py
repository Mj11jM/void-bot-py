import asyncio
import discord
from helpers import confirmationMenu
from bot_index import startExt
from discord.ext import commands

confirm_men = confirmationMenu.ConfirmationMenus

class Admin(commands.Cog):

    """Admin-esque commands."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_guild_permissions(administrator=True)
    async def leave(self, ctx):
        """The bot will leave the server"""
        confirm = await confirm_men('Are you sure you want Void Bot to leave the server?').prompt(ctx)
        if confirm :
            await asyncio.sleep(2)
            await ctx.message.guild.leave()


def setup(bot):
    bot.add_cog(Admin(bot))


